#ifndef  _SCENE_H_
#define  _SCENE_H_
#include "cocos2d.h"
#include "Game.h"


class TitleScene : public cocos2d::CCLayer{
 public:
  virtual bool init();
  static cocos2d::CCScene* scene();
  CREATE_FUNC(TitleScene);
  void gotoGameScene(CCObject* pSender);
  void keyBackClicked();
  void keyMenuClicked();
};

class GameScene : public CCLayer{
 public:
  PGame game;
  virtual bool init();
  static CCScene* scene();
  CREATE_FUNC(GameScene);
  //bool ccTouchBegan(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent);
  void keyBackClicked();
  void keyMenuClicked();
};

#endif // _SCENE_H_

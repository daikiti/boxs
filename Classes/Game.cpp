#include "Game.h"
#include <algorithm> // sort

void addparticle(CCMenuItemImage* item,const CCPoint& pos){
  CCParticleExplosion* p = CCParticleExplosion::create(); // 爆発的なエフェクトの初期化
  p->setDuration(0.01);     // エフェクトが停止するまでの時間 
  p->setSpeed(1000);        // エフェクトの動く速さ
  p->setPosition(ccp(0,0)); // エフェクトの発生地点
  p->setAutoRemoveOnFinish(true); //終了時に自動的に解放
  //int tag = 111;
  item->addChild(p,10);
  //item->scheduleOnce(schedule_selector(Block::deleteeffect),1);
}

Game::~Game(){
  //
}
void Game::touchableimpl(vector<Sortt>& hits,int x,int y){
  for(int i=x-1;i>=0;--i){
    if(this->boxs[y][i]->getVisible()){
      sorttpush(hits,i,y);
      break;
    }
  }

  for(int i=x+1;i<this->yoko;++i){
    if(this->boxs[y][i]->getVisible()){
      sorttpush(hits,i,y);
      break;
    }
  }
  
  for(int i=y-1;i>=0;--i){
    if(this->boxs[i][x]->getVisible()){
      sorttpush(hits,x,i);
      break;
    }
  }
    
  for(int i=y+1;i<this->tate;++i){
    if(this->boxs[i][x]->getVisible()){
      sorttpush(hits,x,i);
      break;
    }
  }

}
void Game::sorttpush(vector<Sortt>& v,int x,int y){
  Sortt tmp;
  tmp.first = this->boxs[y][x]->number;
  tmp.second.first = x;
  tmp.second.second = y;
  v.push_back(tmp);
}

bool Game::cantouch(){
  for(int y=0;y<this->tate;++y){
    for(int x=0;x<this->yoko;++x){
      if(touchable(x,y))return true;
    }
  }
  return false;
}
bool Game::touchable(int x,int y){
  //画面外はタッチ不可
  if(x<0 || y<0 || x>=this->yoko || y>=this->tate){
    return false;
  }
  // 空きマスだけタッチできる
  if(boxs[y][x]->getVisible())return false;

  vector<Sortt> hits;
  touchableimpl(hits,x,y);
  sort(hits.begin(),hits.end());
  if(hits.size()>=4 && hits[0].first == hits[1].first && hits[1].first == hits[2].first && hits[2].first == hits[3].first){
    return true;
  }else if(hits.size() >= 4 && hits[0].first == hits[1].first && hits[2].first == hits[3].first){
    return true;
  }else if(hits.size()>=3 && hits[0].first == hits[1].first && hits[1].first == hits[2].first){
    return true;
  }else if(hits.size()>=3 && hits[2].first == hits[3].first && hits[1].first == hits[2].first){
    return true;
  }else if(hits.size()>=2 && hits[0].first == hits[1].first){
    return true;
  }else if(hits.size()>=2 && hits[1].first == hits[2].first){
    return true;
  }else if(hits.size()>=2 && hits[2].first == hits[3].first){
    return true;
  }
  return false;
}

void Game::disappear(int y,int x){
  this->boxs[y][x]->setVisible(false);
  addparticle(this->boxs[y][x],this->boxs[y][x]->getPosition());
}

bool Game::trytouch(int x,int y){
  if(x<0 || y<0 || x>=this->yoko || y>=this->tate){
    return false;
  }
  
  vector<Sortt> hits;
  touchableimpl(hits,x,y);
  sort(hits.begin(),hits.end());

  if(hits.size()>=4 && hits[0].first == hits[1].first && hits[1].first == hits[2].first && hits[2].first == hits[3].first){
    for(int i=0;i<hits.size();++i){
      this->disappear(hits[i].second.second,hits[i].second.first);
    }
  }else if(hits.size() >= 4 && hits[0].first == hits[1].first && hits[2].first == hits[3].first){
    for(int i=0;i<hits.size();++i){
      this->disappear(hits[i].second.second,hits[i].second.first);
    }
  }else if(hits.size()>=3 && hits[0].first == hits[1].first && hits[1].first == hits[2].first){
    this->disappear(hits[0].second.second,hits[0].second.first);
    this->disappear(hits[1].second.second,hits[1].second.first);
    this->disappear(hits[2].second.second,hits[2].second.first);
  }else if(hits.size()>=3 && hits[2].first == hits[3].first && hits[1].first == hits[2].first){
    this->disappear(hits[1].second.second,hits[1].second.first);
    this->disappear(hits[2].second.second,hits[2].second.first);
    this->disappear(hits[3].second.second,hits[3].second.first);
  }else if(hits.size()>=2 && hits[0].first == hits[1].first){
    this->disappear(hits[0].second.second,hits[0].second.first);
    this->disappear(hits[1].second.second,hits[1].second.first);
  }else if(hits.size()>=2 && hits[1].first == hits[2].first){
    this->disappear(hits[1].second.second,hits[1].second.first);
    this->disappear(hits[2].second.second,hits[2].second.first);
  }else if(hits.size()>=2 && hits[2].first == hits[3].first){
    this->disappear(hits[2].second.second,hits[2].second.first);
    this->disappear(hits[3].second.second,hits[3].second.first);
  }

  return true;
}

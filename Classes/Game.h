#ifndef  _GAME_H_
#define  _GAME_H_
#include "cocos2d.h"
#include "Block.h"
using namespace cocos2d;
using namespace std;

typedef pair<int,pair<int,int> > Sortt;

class Block;
class Game{
 private:
  CCLayer* layer;
  void touchableimpl(vector<Sortt>& v,int x,int y);
  void disappear(int x,int y);
 public:
  virtual ~Game();
  Block* boxs[14][10];
  int tate;
  int yoko;
 Game(CCLayer *l):tate(14),yoko(10),layer(l){}
  bool trytouch(int x,int y);
  bool touchable(int x,int y); // 指定マスがタッチ可能か
  bool cantouch();          // 全マスにタッチ可能な場所があるか
  void sorttpush(vector<Sortt>& v,int x,int y);
};

#endif  // _GAME_H_

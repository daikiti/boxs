#ifndef  _BLOCK_H_
#define  _BLOCK_H_

#include <memory>
#include "Game.h"
#include "cocos2d.h"

class Game;
using namespace std;
using namespace cocos2d;
typedef shared_ptr<Game> PGame;

class Block : public cocos2d::CCMenuItemImage{    
 private:
  bool visible;
 public:
  int x;
  int y;
  int number;

  PGame game;
  virtual ~Block();
  // 生成関数
  static Block* create(int number,CCObject* parent,SEL_MenuHandler selector,int x,int y);
  void onTapThis(cocos2d::CCObject* sender);
  void setGame(PGame gm);
  void setVisible(bool);
  bool getVisible();
  //void deleteeffect();
};

#endif  // _BLOCK_H_

#include "Block.h"

Block::~Block(){
  //
}

//void Block::deleteeffect(){  this->removeChildByTag(111); // todo: magic number}
void Block::setVisible(bool v){
  this->visible=v;
  if(v){
    this->setOpacity(255);
  }else{
    this->setOpacity(0);
  }
}
bool Block::getVisible(){
  return this->visible;
}

void Block::setGame(PGame gm){
  this->game = gm;
}
Block* Block::create(int num,CCObject* parent,SEL_MenuHandler selector,int xx,int yy){
  Block *pRet = new Block();
  const char * images[] = {"rcrect1.png","rcrect2.png","rcrect3.png","rcrect4.png","rcrect5.png","rcrect6.png","rcrecttrans.png"};
  const char*image = images[num];
  if (pRet && pRet->initWithNormalImage(image, image, image, parent, selector)) {
    pRet->autorelease();
    pRet->number=num;
    pRet->x=xx;
    pRet->y=yy;
    if(num!=7-1){
      pRet->visible=true;
    }else{
      pRet->visible=false;
    }
    return pRet;
  }
  CC_SAFE_DELETE(pRet);
  return NULL;
}


/*
  Block* Block::create(const char* image,CCObject* parent,SEL_MenuHandler selector){
  Block *pRet = new Block();
  if (pRet && pRet->initWithNormalImage(image, image, image, parent, selector)) {
  pRet->autorelease();
  return pRet;
  }
  CC_SAFE_DELETE(pRet);
  return NULL;
  }
*/

void Block::onTapThis(cocos2d::CCObject *sender){
  Block* b = dynamic_cast<Block*>(sender);
  char buf[255];
  sprintf(buf,"ontap %d %d %d",b->x,b->y,b->number);
  // こうすると未初期化のような値がかえる
  //sprintf(buf,"ontap %d %d %d",this->x,this->y,this->number);
  //CCMessageBox(buf,"title");
  //CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("effect1.wav");
  
  if(!(b->game->boxs[b->y][b->x]->getVisible())){
    b->game->trytouch(b->x,b->y);
  }
  if(!b->game->cantouch()){
    CCMessageBox("これ以上動かせなくなりました","title");
  }
}




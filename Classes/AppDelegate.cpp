#include "AppDelegate.h"
#include "Scene.h"

using namespace cocos2d;
using namespace std;

//デザイン解像度(iphone4/4s/ipodtouch4世代あたりと一緒)
// portrait(縦画面)
int dr_width = 640;
int dr_height = 960;

AppDelegate::AppDelegate() {}
AppDelegate::~AppDelegate() {}
bool AppDelegate::applicationDidFinishLaunching() {
  CCDirector* pDirector = CCDirector::sharedDirector();
  CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();
  pDirector->setOpenGLView(pEGLView);
  pDirector->setDisplayStats(false);
  pDirector->setAnimationInterval(1.0 / 60);
  CCEGLView::sharedOpenGLView()->setDesignResolutionSize(dr_width, dr_height, kResolutionShowAll);
  CCScene *pScene = TitleScene::scene();
  pDirector->runWithScene(pScene);
  return true;
}

void AppDelegate::applicationDidEnterBackground() {
  CCDirector::sharedDirector()->stopAnimation();
  // if you use SimpleAudioEngine, it must be pause
  // SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

void AppDelegate::applicationWillEnterForeground() {
  CCDirector::sharedDirector()->startAnimation();
  // if you use SimpleAudioEngine, it must resume here
  // SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}

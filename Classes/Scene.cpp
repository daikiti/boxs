#include "Scene.h"
#include "SimpleAudioEngine.h"

extern int dr_width; 
extern int dr_height;

void appexit(){
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT) || (CC_TARGET_PLATFORM == CC_PLATFORM_WP8)
  CCMessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
#else
  CCDirector::sharedDirector()->end();
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif
#endif
}
void TitleScene::keyBackClicked(){
  appexit();
}
void TitleScene::keyMenuClicked(){
}

/*
  bool GameScene::ccTouchBegan(cocos2d::CCTouch* pTouch, cocos2d::CCEvent* pEvent){
  CCMessageBox("gamescene cctouchbegan","title");
  return true;
  }
*/

void GameScene::keyBackClicked(){
  CCScene* nextScene = TitleScene::scene();
  float duration = 1.0f;
  CCScene* pScene = CCTransitionFade::create(duration, nextScene);
  if(pScene){
    CCDirector::sharedDirector()->replaceScene(pScene);
  }

}
void GameScene::keyMenuClicked(){
}

CCScene* TitleScene::scene(){
  CCScene *scene = CCScene::create();
  TitleScene *layer = TitleScene::create();
  scene->addChild(layer);
  return scene;
}



bool TitleScene::init(){
  if ( !CCLayer::init() ) { return false; }
  this->setKeypadEnabled( true );

  CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
  CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
  CCLabelTTF* pLabel = CCLabelTTF::create("Title", "Arial", 24);
  pLabel->setPosition(ccp(dr_width/2,dr_height - pLabel->getContentSize().height*2));
  this->addChild(pLabel, 1);

  CCMenuItemFont *pGotoNextScheneItem = CCMenuItemFont::create( "GAME START", this, menu_selector(TitleScene::gotoGameScene));
  pGotoNextScheneItem->setFontSize(35);
  pGotoNextScheneItem->setFontName("Helvetica");
  CCMenuItemFont *ruleItem = CCMenuItemFont::create( "RULE", this, menu_selector(TitleScene::gotoGameScene));
  ruleItem->setFontSize(35);
  ruleItem->setFontName("Helvetica");
  ruleItem->setPosition(0,ruleItem->getContentSize().height*2);
  CCMenu* pMenu = CCMenu::create(pGotoNextScheneItem, NULL);
  pMenu->addChild(ruleItem,3);
  pMenu->setPosition(dr_width/2 ,dr_height/2);
  this->addChild(pMenu, 2);
  CCParticleSystemQuad* particle = CCParticleSystemQuad::create("par.plist");
  this->addChild(particle,3);
  return true;
}

void TitleScene::gotoGameScene(CCObject* pSender){
  CCScene* nextScene = GameScene::scene();
  float duration = 1.0f;
  CCScene* pScene = CCTransitionFade::create(duration, nextScene);
  CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("effect1.wav");
  if(pScene){
    CCDirector::sharedDirector()->replaceScene(pScene);
  }
}


CCScene* GameScene::scene(){
  CCScene *scene = CCScene::create();
  GameScene *layer = GameScene::create();

  CCSize size = CCDirector::sharedDirector()->getWinSize();
  CCLayerColor* background = CCLayerColor::create(ccc4(255,255,255, 255), size.width,size.height);

  background->setPosition(0,0);
  scene->addChild(background,1);
  scene->addChild(layer,2);
  return scene;
}

bool GameScene::init(){
  if ( !CCLayer::init() ) { return false; }
  CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

  PGame gp(new Game(this));
  this->game = gp;

  // 960x640に64pixelの画像を縦14,横10で配置。
  int pix = 960/15; //64
  int tate = 14;
  int yoko=10;
  for(int y=0;y<tate;++y){
    for(int x=0;x<yoko;++x){
      int rnd = random()%7;
      this->game->boxs[y][x]=Block::create(rnd,this,menu_selector(Block::onTapThis),x,y);
      this->game->boxs[y][x]->setGame(this->game);
      CCMenu* menu = CCMenu::create(this->game->boxs[y][x],NULL);
      menu->setPosition(ccp(pix/2 + x*pix,pix/2 + y*pix));
      this->addChild(menu,3);
    }
  }
  this->setKeypadEnabled( true );
  this->setTouchEnabled(true);
  this->setTouchMode(kCCTouchesOneByOne);
  return true;
}
